#pragma once
#include <functional>



/**
 * Main namespace for the module! nyaa 
 */
namespace adt {
    /**
     * Provide a simple way to overload on types nyaa 
     */
    template <typename... Callables>
    struct overload : Callables... {
        using Callables::operator()...;
        overload(Callables&&... fns) : std::decay_t<Callables>(std::forward<Callables>(fns))... {}
    };

    template <typename... Callables>
    overload (Callables&&...) -> overload <Callables...>;
}


/**
 * adt
 * Copyright (C) 2020 sapient_cogbag
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

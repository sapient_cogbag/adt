#pragma once
#include <adt/version.hpp>
#include <concepts>
#include <type_traits>

namespace adt::concepts {

    namespace _detail {
        template <typename... Ts>
        struct are_unique : std::false_type {};

        template <typename T, typename... Ts>
        struct are_unique <T, Ts...> : std::bool_constant <
            ((!std::is_same_v <T, Ts>) &&...) && are_unique <Ts...>::value
        > {};

        template <>
        struct are_unique <> : std::true_type {};
    }

    template <typename... Ts>
    constexpr inline bool are_unique_v = _detail::are_unique <Ts...>::value;
    
    template <typename... T>
    concept unique = requires {
        are_unique_v<T...>;
    };

}


/**
 * adt
 * Copyright (C) 2020 sapient_cogbag
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

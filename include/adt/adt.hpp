#pragma once
#include <adt/type_wrapper.hpp>
#include <cstdint>


namespace adt {

    /**
     * The main class for Algebraic Data Types nyaa.
     *
     * This is used by simple inheritance. All ADT types use CRTP to create unique
     * types in a simple manner, so every inheritance will start as the following nyaa:
     *
     * ```
     * struct Type : adt <Type, ...>{};
     * ```
     *
     * To create a type that is-a nother type (while being distinct/more-inherited than
     * it, you do the following:
     *
     * ```
     * struct Type : adt <Type, IsA_Type> {};
     * ```
     */
    template <typename... T>
    struct adt;

    /**
     * Specialisation for simple plain pure-type-as-information thingy nyaa
     */
    template <typename Derived>
    struct adt <Derived> {};

    /**
     * Specialisation for builtin types. 
     */
    template <typename T> struct adt <T, std::int8_t> : tws <std::int8_t> {};
    template <typename T> struct adt <T, std::int16_t> : tws <std::int16_t> {};
    template <typename T> struct adt <T, std::int32_t> : tws <std::int32_t> {};
    template <typename T> struct adt <T, std::int64_t> : tws <std::int64_t> {};

    template <typename T> struct adt <T, std::uint8_t> : tws <std::uint8_t> {};
    template <typename T> struct adt <T, std::uint16_t> : tws <std::uint16_t> {};
    template <typename T> struct adt <T, std::uint32_t> : tws <std::uint32_t> {};
    template <typename T> struct adt <T, std::uint64_t> : tws <std::uint64_t> {};

    template <typename T> struct adt <T, bool> : tws <bool> {};
    template <typename T> struct adt <T, float> : tws <float> {};
    template <typename T> struct adt <T, double> : tws <double> {};

    template <typename T> struct adt <T, char8_t> : tws <char8_t> {};
    template <typename T> struct adt <T, char16_t> : tws <char16_t> {};
    template <typename T> struct adt <T, char32_t> : tws <char32_t> {};
    
    /**
     * Pointers nyaa
     */
    template <typename Q, typename T> struct adt <Q, T*> : tws <T*> {};

    /**
     * Specialisation for building from another type :) 
     */
    template <typename Derived, typename IsA>
    struct adt <Derived, IsA> : IsA {};

    /**
     * one_of holds one of a given unique set of items (with decay_t) nya 
     */ 
    



}



/**
 * adt
 * Copyright (C) 2020 sapient_cogbag
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

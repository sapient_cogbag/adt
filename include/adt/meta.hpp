#pragma once
#include <adt/version.hpp>
#include <type_traits>

namespace adt {
    /**
     * Simple metatype lifted to values nyaa
     */
    template <typename T>
    struct metatype {
        using type = T;
    };

    /**
     * Simple metavalue lifted to value 
     */
    template <auto _value>
    struct metavalue {
        using value_type = decltype(_value);
        static constexpr auto value = _value; 
    };
}

namespace adt::concepts {
    namespace _detail {
        /** {{{
         * Check if the given type is an instantiation of the given template nyaa
         *
         * This is the typetemplate (tt) variant nya
         *
         * Until we get Universal Template Parameters (http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p1985r1.pdf)
         * we have to make different variants of this for all-type, all-template type, and all value-type versions nyaa
         */
        template <typename T, template <typename...> typename Template>
        struct is_tt_instantiation_of : std::false_type {};

        template <
            typename... Ts,
            template <typename...> typename Template
        >
        struct is_tt_instantiation_of <Template <Ts...>, Template> : std::true_type {};

        template <typename T, template <typename...> typename Template>
        inline constexpr bool is_tt_instantiation_of_v = is_tt_instantiation_of<
            T, Template
        >::value;

        // }}} 



        /** {{{
         * Check if the given type is an instantiation of the given template nyaa
         *
         * This is the valuetemplate (vt) variant nya
         *
         * Until we get Universal Template Parameters (http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p1985r1.pdf)
         * we have to make different variants of this for all-type, all-template type, and all value-type versions nyaa
         */
        template <typename T,  template <auto...> typename Template>
        struct is_vt_instantiation_of : std::false_type {};

        template <
            auto... Vs,
            template <auto...> typename Template
        >
        struct is_vt_instantiation_of <Template <Vs...>, Template> : std::true_type {};

        template <typename T, template <auto...> typename Template>
        inline constexpr bool is_vt_instantiation_of_v = is_vt_instantiation_of<
            T, Template
        >::value;

        // }}}
        
         

        /** {{{
         * Check if the given type is an instantiation of the given template nyaa
         *
         * This is the typetemplatetemplate (ttt) variant nya
         *
         * Until we get Universal Template Parameters (http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p1985r1.pdf)
         * we have to make different variants of this for all-type, all-template type, and all value-type versions nyaa
         *
         * Note that due to this lack of universal template parameters nyaa, this is only
         * compatible with template types taking type parameters.
         */
        template <typename T,  template <template <typename...> typename...> typename Template>
        struct is_ttt_instantiation_of : std::false_type {};

        template <
            template <typename...> typename... TTs,
            template <template <typename...> typename...> typename Template
        >
        struct is_ttt_instantiation_of <Template <TTs...>, Template> : std::true_type {};

        template <typename T, template <template <typename...> typename...> typename Template>
        inline constexpr bool is_ttt_instantiation_of_v = is_ttt_instantiation_of<
            T, Template
        >::value;

        // }}} 
    }
    
    /**
     * Check if a type is an instantiation of the given template nyaa
     *
     * This is the variant for type parameters
     */
    template <typename T, template <typename...>  typename Template>
    concept tt_instantiation_of = _detail::is_tt_instantiation_of_v <T, Template>;

    /**
     * Check if a type is an instantiation of the given template nyaa
     *
     * This is the variant for value parameters
     */
    template <typename T, template <auto...>  typename Template>
    concept vt_instantiation_of = _detail::is_vt_instantiation_of_v <T, Template>;


    /**
     * Check if a type is an instantiation of the given template nyaa
     *
     * This is the variant for type-template template parameters
     */
    template <typename T, template <template <typename...> typename...>  typename Template>
    concept ttt_instantiation_of = _detail::is_ttt_instantiation_of_v <T, Template>;

    template <typename T>
    concept is_metavalue = vt_instantiation_of<T, metavalue>;

    template <typename T>
    concept is_metatype = tt_instantiation_of<T, metatype>;

    template <typename MetavalueType, auto compare>
    concept metavalue_equal = requires {
        is_metavalue<MetavalueType>;
        MetavalueType::value == compare;
    };

}

namespace adt {
    template <concepts::is_metatype MT>
    using metatype_type_t = typename MT::type;
}

/**
 * adt
 * Copyright (C) 2020 sapient_cogbag
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file 
 *
 * This file defines MATCHING values given certain conditions nya.
 */
#pragma once
#include <type_traits>
#include <optional>
#include <adt/concepts.hpp>
#include <concepts>
#include <adt/meta.hpp>

namespace adt {

    template <typename... SubMatchers>
    struct ma;

    /**
     * Namespace for concepts required for matching a type nyaa
     */
    namespace matchconcepts {
        /**
         * Concept for a type being some kind of varying type that can be matched
         * on the type it holds via this interface.
         *
         * Attempts to extract by value are controlled not by this but by the matching
         * bindings nyaa
         *
         * Details:
         * * holds(inst, metatype<TYPE>()) must produce a bool indicating if the given
         *   variant holds the given type in the metatype nya. This should work even
         *   if the type provided is invalid.
         * * extract(inst, metatype<TYPE>()) must extract the given type as something convertible to a:
         *   - constref if inst is a constref
         *   - ref if inst is a nonconst ref
         *   - value if inst is an rvalue ref
         *   * Note that extract does NOT require you to check for validity in the type
         *     in question, at least for this concept ~ functions in this library that 
         *     use it check for holds() beforehand nya
         *   Not required to work if the type is invalid nya.
         * * T::count is metavalue <# of types> nya.
         * * T::type_index(metavalue<index>()) -> metatype<type-at-index>();
         *
         * Design motivation:
         *
         *  * inheritance from a varying type must automatically confer varying-ness
         *    without any extra user effort nya
         *
         *  * this means that holds- and extraction-, which act on instances of types, 
         *    should be free functions so that all things inheriting from the first
         *    argument get passed through in that way nya.
         *
         *  * the compile-time known values and calculations must use things passed down
         *    from parent type to child type. For values, this can just be static constexpr
         *    value (count). For calculations this gets more complex, but we can get away
         *    with this by lifting types to values in a static constexpr value that is a lambda
         *    that works on metatypes nya.
         *
         *  * this concept tries to sanity check this as much as possible, if there
         *    are a nonzero # of types nyaa (we do not require extract to work on invalid
         *    types like with `hold`)
         *
         */
        template <typename T>
        concept matchable_varying = requires (const T inst) {
            { holds(inst, metatype<int>()) } -> std::same_as <bool>;
            { T::count } -> concepts::is_metavalue;
            // nonzero counts should allow type retreival nya
            concepts::metavalue_equal <decltype(T::count), 0> || requires (
                    metavalue<0> zero_mv,
                    const T& constant_tref,
                    T& tref,
                    T&& trvalue
                    
                ) {
                { T::type_index(zero_mv) } -> concepts::is_metatype;
                { extract(constant_tref, T::type_index(zero_mv)) } -> std::convertible_to<
                    const metatype_type_t<decltype(T::type_index(zero_mv))>&
                >;
                { extract(tref, T::type_index(zero_mv)) } -> std::convertible_to<
                    metatype_type_t<decltype(T::type_index(zero_mv))>&                
                >;
                { extract(trvalue, T::type_index(zero_mv)) } -> std::convertible_to<
                    metatype_type_t<decltype(T::type_index(zero_mv))>
                >;
                 
            };
        };

    }


    /**
     * Indicator type for containing one of these types.
     */
    template <typename... one_of>
    struct holds { 
        
    };

    /**
     * This structure fundamentally allows for definitions of matchers, 
     * primarily stored in the type system nya.
     *
     * Matching is inspired here at least somewhat by the proposal for c++
     * pattern matching: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1371r1.pdf
     * (with reduced features nya).
     *
     * In particular matching is checked in the order of being provided
     */
    template <typename... SubMatchers>
    struct ma { 
         
    };

}


/**
 * adt
 * Copyright (C) 2020 sapient_cogbag
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

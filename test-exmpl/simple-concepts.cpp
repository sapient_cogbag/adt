#include <adt/concepts.hpp>


int main(int argc, char** argv) {
    static_assert(adt::concepts::are_unique_v <>, "Empty list must be unique");
    static_assert(adt::concepts::are_unique_v <int>, "Single type must be unique");
    static_assert(adt::concepts::are_unique_v <int, float>, "Multitypes must be unique nyaa");
    static_assert(adt::concepts::are_unique_v <int, float, bool>);
    static_assert(!adt::concepts::are_unique_v <int, int, float>);
    static_assert(!adt::concepts::are_unique_v <int, float, float>);

}


/**
 * adt
 * Copyright (C) 2020 sapient_cogbag
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

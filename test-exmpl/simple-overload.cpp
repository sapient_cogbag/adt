#include <adt/overload.hpp>
#include <iostream>
#include <variant>
#include <adt/type_wrapper.hpp>


struct A{};
struct B{};

struct C : adt::tws <int> {};

template <typename T>
auto testfn(adt::tws<T> d) {
    std::cout << d.raw_data << std::endl;
}


int main(int argc, char** argv) {
    
    using multi = std::variant<A, B>;
    const auto components = std::vector<multi> {
        A{}, B{}, B{}, A{}, A{}, B{}, B{}, B{}, A{}, B{}, A{}, A{}
    };

    for (const auto alpha : components) {
        std::visit( 
            adt::overload {
                [](A a){
                    std::cout << "Aaaaaayo" << std::endl;
                },
                [](B b){
                    std::cout << "bbbbbbbb" << std::endl;
                }
            }, alpha
        );
    }

    testfn(C());

    return 0;
}



/**
 * adt
 * Copyright (C) 2020 sapient_cogbag
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

